import React, { Component } from 'react'
import { Provider } from 'react-redux'
import Cleaner from './src/modules/Cleaner/components/Home'
import HomeOwner from './src/modules/HomeOwner/components/Home'
import Txn from './src/modules/cashSplitting/containers/TransactionList'
import Home from './src/components/Home'
import store from './src/redux/store'
import * as Expo from 'expo'

class App extends Component {
  constructor () {
    super()
    this.state = {
      isReady: false
    }
  }

  componentWillMount () {
    this.loadFonts()
  }

  async loadFonts () {
    await Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('@expo/vector-icons/fonts/Ionicons.ttf'),
      MaterialCommunityIcons: require('@expo/vector-icons/fonts/MaterialCommunityIcons.ttf')
    })
    this.setState({ isReady: true })
    console.log('loaded')
  }

  render () {
    if (!this.state.isReady) {
      return <Expo.AppLoading />
    }
    return (
      <Provider store={store}>
        <Home />
      </Provider>
    )
  }
}

export default App
