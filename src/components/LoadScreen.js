import React from 'react'
import { Image, View, StyleSheet } from 'react-native'
import * as colors from '../constants/colors'

export const LoadingScreen = () => (
  <View style={styles.container}>
    <Image
      style={styles.image}
      source={require('../../finding.gif')} />
  </View>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.BACKGROUND,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  image: {
    resizeMode: 'center'
  }
})

export default LoadingScreen
