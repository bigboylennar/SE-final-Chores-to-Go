import React from 'react'
import AppNavigator from '../modules/Navigation/containers/AppNavigator'
import { Root } from 'native-base'

export const Home = () => {
  return (
    <Root>
      <AppNavigator/>
    </Root>
  )
}

export default Home
