export const HOUSEKEEPING = {name: 'Housekeeping', icon: 'broom', constName: 'HOUSEKEEPING'}
export const LAUNDRY = {name: 'Laundry', icon: 'washing-machine', constName: 'LAUNDRY'}
export const DISH_WASHING = {name: 'Dish Washing', icon: 'pot', constName: 'DISH_WASHING'}
export const CAR_WASHING = {name: 'Car Washing', icon: 'car-wash', constName: 'CAR_WASHING'}
export const GARDENING = {name: 'Gardening', icon: 'flower', constName: 'GARDENING'}
export const PREPARE_MEALS = {name: 'Prepare Meals', icon: 'stove', constName: 'PREPARE_MEALS'}
