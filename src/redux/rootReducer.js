import { combineReducers } from 'redux'

// import transactions from '../modules/cashSplitting/reducers/transactionsReducer'
import transactions from '../modules/cashSplitting/reducers/txn-reducer'
import checkboxes from '../modules/HomeOwner/reducers/checkboxReducer'
import navReducer from '../modules/Navigation/reducers/navReducer'
import { reducer as formReducer } from 'redux-form'
import currentUser from '../modules/auth/reducers/userReducer'
import matchedCleaner from '../modules/HomeOwner/reducers/matchedUserReducer'
import status from '../modules/HomeOwner/reducers/statusReducer'

const rootReducer = combineReducers({
  transactions,
  checkboxes,
  nav: navReducer,
  form: formReducer,
  currentUser,
  matchedCleaner,
  status
})

export default rootReducer
