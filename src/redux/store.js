import { createStore, applyMiddleware, compose } from 'redux'
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers'
import thunk from 'redux-thunk'

import reducers from './rootReducer'

const navigationMiddleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav
)

const enhancer = compose(applyMiddleware(thunk, navigationMiddleware))

export default createStore(reducers, enhancer)
