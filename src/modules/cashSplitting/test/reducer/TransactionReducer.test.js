import transactionsReducer from '../../reducers/transactionsReducer'
import { SHOW_ALL_TRANSACTIONS } from '../../actions/actionTypes'

describe('>>> Reducer: TRANSACTIONS', () => {
  const transactions = [
    {
      id: '000000123121',
      date: new Date('Wed Nov 25 2015 08:00:00 GMT+0800'),
      cleanerFee: 300
    },
    {
      id: '000000123221',
      date: new Date('Wed sep 25 2016 09:00:00 GMT+0800'),
      cleanerFee: 400
    },
    {
      id: '000000123222',
      date: new Date('Wed Oct 23 2017 10:00:00 GMT+0800'),
      cleanerFee: 450
    },
    {
      id: '000000154321',
      date: new Date('Wed Mar 21 2018 08:00:00 GMT+0800'),
      cleanerFee: 275
    }
  ]

  it('+++ update transactions in state correctly', () => {
    let state = {}
    const action = {
      type: SHOW_ALL_TRANSACTIONS,
      payload: transactions
    }
    const expected = {transactions: transactions}
    state = transactionsReducer(state, action)
    expect(state).toEqual(expected)
  })

  it('+++ return state if action type is not valid', () => {
    let state = {}
    const action = {
      type: 'WRONG_TYPE',
      payload: transactions
    }
    const expected = {}
    state = transactionsReducer(state, action)
    expect(state).toEqual(expected)
  })
})
