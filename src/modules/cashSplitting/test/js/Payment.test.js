import * as Payment from '../../js/Payment'

describe('>>> Pure Functions: Payment', () => {
  const transaction = {
    hoursWorked: 6,
    cleanerFee: 300
  }
  const cleaner = {
    hourlyRate: 50
  }

  it('+++  computes cleaner fee', () => {
    const expected = 300
    const cleanerFee = Payment.getCleanerFee(transaction, cleaner)
    expect(cleanerFee).toEqual(expected)
  })

  it('+++  computes costumer payment', () => {
    const expected = 345
    const payment = Payment.getPlusFifteenPercent(transaction)
    expect(payment).toEqual(expected)
  })

  it('+++  computes txn fee', () => {
    const expected = 11
    const txnFee = Payment.getTransactionFee(transaction)
    expect(txnFee).toEqual(expected)
  })

  it('+++  computes profit', () => {
    const expected = 34
    const profit = Payment.getProfit(transaction)
    expect(profit).toEqual(expected)
  })
})
