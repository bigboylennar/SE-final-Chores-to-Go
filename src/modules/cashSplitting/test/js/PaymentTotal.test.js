import * as PaymentTotal from '../../js/PaymentTotal'

describe('>>> Pure Functions: Payment Total', () => {
  const transactions = [
    {
      id: '000000123121',
      date: new Date('Wed Nov 25 2015 08:00:00 GMT+0800'),
      cleanerFee: 300
    },
    {
      id: '000000123221',
      date: new Date('Wed sep 25 2016 09:00:00 GMT+0800'),
      cleanerFee: 400
    },
    {
      id: '000000123222',
      date: new Date('Wed Oct 23 2017 10:00:00 GMT+0800'),
      cleanerFee: 450
    },
    {
      id: '000000154321',
      date: new Date('Wed Mar 21 2018 08:00:00 GMT+0800'),
      cleanerFee: 275
    }
  ]

  it('+++  computes payment summary for cash splitting', () => {
    const expected = {
      cleanerFee: 1425,
      plusFifteenPercent: 1640,
      txnFee: 51,
      profit: 164
    }
    const summary = PaymentTotal.getTotal(transactions)
    expect(summary).toEqual(expected)
  })
})
