import React from 'react'
import CashSplit from '../../components/CashSplit'
import CashSplitSummary from '../../components/CashSplitSummary'
import { Row } from 'react-native-easy-grid'
import {shallow} from 'enzyme'

describe('>>> Component: CASH SPLIT SUMMARY', () => {
  let wrapper
  const transaction = {
    id: '000000123121',
    date: new Date('Wed Nov 25 2015 08:00:00 GMT+0800'),
    cleanerFee: 300
  }

  beforeEach(() => {
    wrapper = shallow(
      <CashSplitSummary transaction={transaction} />
    )
  })

  it('+++ renders the CashSplitSummary', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('+++ renders the CashSplitSummary as expected', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('+++ renders 4 <Row />', () => {
    expect(wrapper.find(Row)).toHaveLength(4)
  })

  it('+++ renders 4 <CashSplit />', () => {
    expect(wrapper.find(CashSplit)).toHaveLength(4)
  })
})
