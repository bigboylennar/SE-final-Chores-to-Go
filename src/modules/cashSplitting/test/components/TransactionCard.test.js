import React from 'react'
import TransactionCard from '../../components/TransactionCard'
import CashSplitSummary from '../../components/CashSplitSummary'
import { CardItem } from 'native-base'
import {shallow} from 'enzyme'

describe('>>> Component: TRANSACTION CARD', () => {
  let wrapper
  const transaction = {
    id: '000000123121',
    date: new Date('Wed Nov 25 2015 08:00:00 GMT+0800'),
    cleanerFee: 300
  }

  beforeEach(() => {
    wrapper = shallow(
      <TransactionCard transaction={transaction} />
    )
  })

  it('+++ renders the CashSplitSummary', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('+++ renders the CashSplitSummary as expected', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('+++ renders 2 <CardItem />', () => {
    expect(wrapper.find(CardItem)).toHaveLength(2)
  })

  it('+++ renders 1 <CashSplitSummary />', () => {
    expect(wrapper.find(CashSplitSummary)).toHaveLength(1)
  })
})
