import React from 'react'
import CashSplit from '../../components/CashSplit'
import { Col } from 'react-native-easy-grid'
import { Text } from 'native-base'
import {shallow} from 'enzyme'

describe('>>> Component: CASH SPLIT', () => {
  let wrapper
  const name = 'TF'
  const value = 700

  beforeEach(() => {
    wrapper = shallow(
      <CashSplit name={name} value={value}/>
    )
  })

  it('+++ renders the Cash Split', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('+++ renders the Cash Split as expected', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('+++ renders 2 <Col />', () => {
    expect(wrapper.find(Col)).toHaveLength(2)
  })

  it('+++ renders 2 <Text />', () => {
    expect(wrapper.find(Text)).toHaveLength(2)
  })
})
