import React from 'react'
import ConnectedTransactionList, {TransactionList} from '../../containers/TransactionList'
import CashSplitTotal from '../../components/CashSplitTotal'
import TransactionCard from '../../components/TransactionCard'
import configureStore from 'redux-mock-store'
import {shallow} from 'enzyme'

describe('>>> Containers: TRANSACTION LIST', () => {
  const initialState = {
    transactions: [
      {
        id: '000000123121',
        date: new Date('Wed Nov 25 2015 08:00:00 GMT+0800'),
        cleanerFee: 300
      },
      {
        id: '000000123221',
        date: new Date('Sep 25 2016 09:00:00 GMT+0800'),
        cleanerFee: 400
      },
      {
        id: '000000123222',
        date: new Date('Oct 23 2017 10:00:00 GMT+0800'),
        cleanerFee: 450
      },
      {
        id: '000000154321',
        date: new Date('Mar 21 2018 08:00:00 GMT+0800'),
        cleanerFee: 275
      }
    ]
  }
  const mockStore = configureStore()
  let store
  let container
  let wrapper

  beforeEach(() => {
    store = mockStore(initialState)
    container = shallow(<ConnectedTransactionList store={store} />)
    wrapper = shallow(<TransactionList transactions={initialState.transactions}/>)
  })

  it('+++ renders the transaction list', () => {
    expect(container).toHaveLength(1)
  })

  it('+++ render transaction list correctly', () => {
    expect(container).toMatchSnapshot()
  })

  it('+++ check Prop matches with initialState', () => {
    expect(container.prop('transactions')).toEqual(initialState.transactions)
  })

  it('+++ renders 4 <TransactionCard />', () => {
    expect(wrapper.find(TransactionCard)).toHaveLength(4)
  })

  it('+++ renders <CashSplitTotal />', () => {
    expect(wrapper.find(CashSplitTotal)).toHaveLength(1)
  })
})
