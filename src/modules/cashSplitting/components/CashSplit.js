import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'
import { Grid, Col } from 'react-native-easy-grid'
import { Text, Body } from 'native-base'

const CashSplit = ({name, value}) => (
  <Grid style={styles.grid}>
    <Col style={styles.col}>
      <Body>
        <Text>{name}</Text>
      </Body>
    </Col>
    <Col style={styles.col}>
      <Body>
        <Text style={styles.value}>{value}</Text>
      </Body>
    </Col>
  </Grid>
)

CashSplit.propTypes = {
  name: PropTypes.string,
  value: PropTypes.number
}

const styles = StyleSheet.create({
  value: {
    alignSelf: 'flex-end'
  },
  col: {
    paddingLeft: 7,
    paddingRight: 7
  },
  grid: {
    paddingTop: 2,
    paddingBottom: 2
  }
})

export default CashSplit
