import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'
import { Card, CardItem, Text, Left, Body, H3 } from 'native-base'
import CashSplitSummary from '../components/CashSplitSummary'

const TransactionCard = ({ transaction }) => (
  <Card>
    <CardItem header style={styles.header}>
      <Left>
        <Body>
          <H3 style={styles.transaction}>{transaction.id}</H3>
          <Text note style={styles.note}>
            {transaction.date.toLocaleString()}
          </Text>
        </Body>
      </Left>
    </CardItem>
    <CardItem>
      <CashSplitSummary transaction={transaction} />
    </CardItem>
  </Card>
)

const colors = {
  header: '#f7f7f7',
  text: '#0d0d0d',
  note: '#333333'
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.header
  },
  transaction: {
    color: colors.text,
    padding: 2
  },
  note: {
    color: colors.note,
    padding: 2
  }
})

TransactionCard.propTypes = {
  transaction: PropTypes.object.isRequired
}

export default TransactionCard
