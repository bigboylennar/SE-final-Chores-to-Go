import React from 'react'
import PropTypes from 'prop-types'
import { Grid, Row } from 'react-native-easy-grid'
import * as Payment from '../js/Payment'
import CashSplit from '../components/CashSplit'

const CashSplitSummary = ({transaction}) => (
  <Grid>
    <Row>
      <CashSplit
        name={'Cleaner Fee'}
        value={transaction.cleanerFee} />
    </Row>
    <Row>
      <CashSplit
        name={'Payment' }
        value={Payment.getPlusFifteenPercent(transaction)} />
    </Row>
    <Row>
      <CashSplit
        name={'3% Transaction Fee'}
        value={Payment.getTransactionFee(transaction)} />
    </Row>
    <Row>
      <CashSplit
        name={'Profit'}
        value={Payment.getProfit(transaction)} />
    </Row>
  </Grid>
)

CashSplitSummary.propTypes = {
  transaction: PropTypes.object.isRequired
}

export default CashSplitSummary
