import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text } from 'react-native'
import { Footer, FooterTab, Body } from 'native-base'
import { getTotal } from '../js/PaymentTotal'

const CashSplitTotal = ({ transactions }) => {
  const summary = getTotal(transactions)
  return (
    <Footer style={styles.footer}>
      <FooterTab style={styles.footer}>
        <Body>
          <Text style={styles.text}>Cleaner Fee</Text>
          <Text style={[styles.total, styles.text]}>{summary.cleanerFee}</Text>
        </Body>
        <Body>
          <Text style={styles.text}>Payment</Text>
          <Text style={[styles.total, styles.text]}>{summary.plusFifteenPercent}</Text>
        </Body>
        <Body>
          <Text style={styles.text}>Transaction Fee</Text>
          <Text style={[styles.total, styles.text]}>{summary.txnFee}</Text>
        </Body>
        <Body>
          <Text style={styles.text}>Profit</Text>
          <Text style={[styles.total, styles.text]}>{summary.profit}</Text>
        </Body>
      </FooterTab>
    </Footer>
  )
}

const colors = {
  text: '#ffffff',
  footer: '#000000'
}

const styles = StyleSheet.create({
  text: {
    color: colors.text
  },
  footer: {
    backgroundColor: colors.footer,
    height: 100
  },
  total: {
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'center'
  }
})

CashSplitTotal.propTypes = {
  transactions: PropTypes.array.isRequired
}

export default CashSplitTotal
