import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'
import { Container, Content, Footer } from 'native-base'
import TransactionCard from '../components/TransactionCard'
import CashSplitTotal from '../components/CashSplitTotal'

export class TransactionList extends Component {
  renderList () {
    return this.props.transactions.map((transaction) => {
      return (
        <TransactionCard
          key={transaction.id}
          transaction={transaction} />
      )
    })
  }

  render () {
    return (
      <Container style={styles.container}>
        <Content>
          {this.renderList()}
        </Content>
        <CashSplitTotal
          transactions={this.props.transactions} />
      </Container>
    )
  }
}

TransactionList.propTypes = {
  transactions: PropTypes.array.isRequired
}

function mapStateToProps (state) {
  return {
    transactions: state.transactions
  }
}

const colors = {
  bgColor: '#e6e6e6'
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.bgColor
  }
})

export default connect(mapStateToProps)(TransactionList)
