import * as Payment from './Payment'

export const getTotal = (transactions) => {
  const summary = {
    cleanerFee: 0,
    plusFifteenPercent: 0,
    txnFee: 0,
    profit: 0
  }

  transactions.map(transaction => {
    summary.cleanerFee += transaction.cleanerFee
    summary.plusFifteenPercent += Payment.getPlusFifteenPercent(transaction)
    summary.txnFee += Payment.getTransactionFee(transaction)
    summary.profit += Payment.getProfit(transaction)
  })

  return summary
}
