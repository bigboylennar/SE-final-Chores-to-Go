export const getCleanerFee = (transaction, cleaner) => {
  return Math.ceil(transaction.hoursWorked * cleaner.hourlyRate)
}

export const getPlusFifteenPercent = (transaction) => {
  return Math.ceil(1.15 * transaction.cleanerFee)
}

export const getTransactionFee = (transaction) => {
  return Math.ceil(0.03 * getPlusFifteenPercent(transaction))
}

export const getProfit = (transaction) => {
  return getPlusFifteenPercent(transaction) -
    transaction.cleanerFee -
    getTransactionFee(transaction)
}
