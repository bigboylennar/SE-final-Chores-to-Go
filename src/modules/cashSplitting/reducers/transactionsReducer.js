import { SHOW_ALL_TRANSACTIONS } from '../actions/actionTypes'

const transactions = (state = {}, action) => {
  switch (action.type) {
    case SHOW_ALL_TRANSACTIONS:
      return {...state, transactions: action.payload}
    default:
      return state
  }
}

export default transactions
