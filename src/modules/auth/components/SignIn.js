import React from 'react'
import { Content, Form, Button, Text } from 'native-base'
import { reduxForm, Field } from 'redux-form'
import styles from '../styles/input'
import InputComponent from '../components/FieldInput'

export const SignIn = (props) => {
  const { handleSubmit, signInHandler } = props
  return (
    <Content style={styles.content}>
      <Form>
        <Field
          name='email'
          component={InputComponent}
          icon='at'
          label='Email'
        />
        <Field
          name='password'
          component={InputComponent}
          icon='lock'
          label='Password'
          secure
        />
      </Form>
      <Button full
        style={styles.button}
        onPress={handleSubmit(signInHandler)}>
        <Text style={styles.buttonText}>
          Sign In
        </Text>
      </Button>
    </Content>
  )
}

export default reduxForm({ form: 'signIn' })(SignIn)
