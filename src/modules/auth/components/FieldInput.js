import React from 'react'
import { Item, Input, Label, Text } from 'native-base'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'
import PropTypes from 'prop-types'

export const FieldInput = ({ input, label, icon, secure, meta: { error, touched } }) => {
  const hasError = error !== undefined
  return (
    <Item error={hasError} floatingLabel>
      <Label>
        {icon ? <Icon name={icon}/> : ''}
        {label}
      </Label>
      <Input
        onChangeText={input.onChange}
        secureTextEntry={secure}/>
    </Item>
  )
}

FieldInput.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object,
  error: PropTypes.string,
  label: PropTypes.string,
  icon: PropTypes.string,
  secure: PropTypes.bool
}

export default FieldInput
