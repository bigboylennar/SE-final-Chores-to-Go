import React from 'react'
import { Content, Form, Button, Text, Grid, Col } from 'native-base'
import { reduxForm, Field } from 'redux-form'
import styles from '../styles/input'
import InputComponent from '../components/FieldInput'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'

export const SignUp = (props) => {
  const { handleSubmit, signUpHandler } = props
  return (
    <Content style={styles.content}>
      <Form>
        <Field
          name='email'
          component={InputComponent}
          icon='at'
          label='Email'
        />
        <Field
          name='password'
          component={InputComponent}
          icon='lock'
          label='Password'
          secure
        />
        <Field
          name='confirmPassword'
          component={InputComponent}
          icon='lock'
          label='Confirm Password'
          secure
        />
        <Text style={styles.title}>
          <Icon name='account-circle' style={styles.title}/>
          Name
        </Text>
        <Grid>
          <Col style={styles.firstName}>
            <Field
              name='firstName'
              component={InputComponent}
              label='First Name'
            />
          </Col>
          <Col style={styles.lastName}>
            <Field
              name='lastName'
              component={InputComponent}
              label='Last Name'
            />
          </Col>
        </Grid>
        <Text style={styles.title}>
          <Icon name='map-marker' style={styles.title}/>
          Address
        </Text>
        <Field
          name='preciseLoc'
          component={InputComponent}
          label='House No., Street, Brgy'
        />
        <Field
          name='district'
          component={InputComponent}
          label='District'
        />
        <Field
          name='city'
          component={InputComponent}
          label='City'
        />
        <Field
          name='province'
          component={InputComponent}
          label='Province'
        />
        <Field
          name='phoneNo'
          component={InputComponent}
          icon='cellphone'
          label='Phone Number'
        />
      </Form>
      <Button full
        style={styles.button}
        onPress={handleSubmit(signUpHandler)}>
        <Text style={styles.buttonText}>
          Register
        </Text>
      </Button>
    </Content>
  )
}

export default reduxForm({
  form: 'signUp'
})(SignUp)
