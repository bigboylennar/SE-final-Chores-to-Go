import {db, auth} from '../../../config/firebase'
import showToast from '../../js/showToast'
import * as actions from './actionCreators'

export const validateSignUp = values => {
  const { email, password, confirmPassword, firstName,
    lastName, preciseLoc, district, city, province, phoneNo
  } = values

  if (!email || !password || !confirmPassword || !firstName ||
    !lastName || !preciseLoc || !province || !phoneNo) {
    throw new Error('Field Required')
  }
  if (password !== confirmPassword) {
    throw new Error('Password doesn\'t match')
  }
  if (!district && !city) {
    throw new Error('Fill at least one field. District or City')
  }
}

export const validateSignIn = values => {
  const { email, password } = values
  if (!email || !password) {
    throw new Error('Field Required')
  }
}

export const signUpUser = (values) => {
  return async (dispatch, getState) => {
    try {
      dispatch(actions.requestSignin())
      validateSignUp(values)
      const { email, password, firstName, lastName,
        preciseLoc, district, city, province, phoneNo
      } = values
      await auth.createUserWithEmailAndPassword(email, password)
      if (auth.currentUser) {
        const userId = String(auth.currentUser.uid)
        const userRef = await db.ref('/users').child(userId)
        await userRef.set({
          firstName,
          lastName,
          phoneNo,
          address: {
            preciseLoc,
            province,
            district: district === undefined ? '' : district,
            city: city === undefined ? '' : city
          },
          online: true
        })
        dispatch(actions.signupSuccess(userId))
        showToast('Sign Up Succesful')
      }
    } catch (error) {
      dispatch(actions.signinFailed(error))
      showToast(error.toString())
      console.log(error)
    }
  }
}

export const signInUser = (values) => {
  return async (dispatch, getState) => {
    try {
      const { email, password } = values
      validateSignIn(values)
      dispatch(actions.requestSignin())
      await auth.signInWithEmailAndPassword(email, password)
      if (auth.currentUser) {
        const userId = auth.currentUser.uid
        const statusRef = await db.ref('users/' + userId + '/online')
        await statusRef.set(true)
        const userRef = await db.ref('users/' + userId)
        const snapshot = await userRef.once('value')
        const currentUser = {
          uid: userId,
          role: (snapshot.val() && snapshot.val().role)
        }
        dispatch(actions.signinSuccess(currentUser))
        showToast('Sign In Succesful')
      }
    } catch (error) {
      dispatch(actions.signinFailed(error))
      showToast('Wrong Email/Password')
      console.log(error)
    }
  }
}

export const signout = () => {
  return async (dispatch, getState) => {
    try {
      await auth.signOut()
      const currentUser = getState().currentUser
      const statusRef = await db.ref('users/' + currentUser + '/online')
      await statusRef.set(false)
      dispatch(actions.signoutSuccess())
      showToast('Sign Out Succesful')
    } catch (error) {
      dispatch(actions.signoutFailed(error))
      showToast(error.toString())
    }
  }
}

export const selectCleanerRole = () => {
  return async (dispatch, getState) => {
    try {
      const currentUser = getState().currentUser.uid
      const userRoleRef = await db.ref('users/' + currentUser + '/role')
      await userRoleRef.set('CLEANER')
      dispatch(actions.selectRoleCleaner())
    } catch (error) {
      showToast(error.toString())
    }
  }
}

export const selectHomeOwnerRole = () => {
  return async (dispatch, getState) => {
    try {
      const currentUser = getState().currentUser.uid
      const userRoleRef = await db.ref('users/' + currentUser + '/role')
      await userRoleRef.set('HOME_OWNER')
      dispatch(actions.selectRoleHomeOwner())
    } catch (error) {
      showToast(error.toString())
    }
  }
}

export const selectSkills = () => {
  return async (dispatch, getState) => {
    try {
      console.log('state', getState())
      const userId = getState().currentUser.uid
      const {checkboxes} = getState()
      const userSkillsRef = await db.ref('users/' + userId + '/skills')
      await userSkillsRef.set([...checkboxes])
      const userRef = await db.ref('users/' + userId)
      const snapshot = await userRef.once('value')
      const currentUser = {
        uid: userId,
        role: (snapshot.val() && snapshot.val().role)
      }
      dispatch(actions.selectSkills(currentUser))
    } catch (error) {
      showToast(error.toString())
    }
  }
}
