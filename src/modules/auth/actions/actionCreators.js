import * as actions from './actionTypes'

export const signinSuccess = ({uid, role}) => ({
  type: actions.SIGNIN_SUCCESS,
  uid,
  role
})

export const signinFailed = (error) => ({
  type: actions.SIGNIN_FAILED,
  error: error
})

export const signupSuccess = (uid) => ({
  type: actions.SIGNUP_SUCCESS,
  uid
})

export const requestSignin = () => ({
  type: actions.REQUEST_SIGNIN
})

export const signoutSuccess = () => ({
  type: actions.SIGNOUT_SUCCESS
})

export const signouFailed = (error) => ({
  type: actions.SIGNOUT_FAILED,
  error: error
})

export const selectRoleCleaner = () => ({
  type: actions.SELECT_ROLE_CLEANER
})

export const selectRoleHomeOwner = () => ({
  type: actions.SELECT_ROLE_HOME_OWNER
})

export const selectSkills = ({uid, role}) => ({
  type: actions.SELECT_SKILLS,
  uid,
  role
})
