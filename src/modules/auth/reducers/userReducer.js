import * as actions from '../actions/actionTypes'

const user = (state = { loggedIn: false }, action) => {
  switch (action.type) {
    case actions.SIGNIN_SUCCESS:
    case actions.SIGNUP_SUCCESS:
    case actions.SELECT_SKILLS:
      return { ...state, loggedIn: true, uid: action.uid }
    case actions.SIGNOUT_SUCCESS:
      return { ...state, loggedIn: false }
    default:
      return state
  }
}

export default user
