import * as colors from '../../../constants/colors'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  headerTitle: {
    color: colors.WHITE,
    fontSize: 25,
    fontWeight: 'bold'
  },
  tab: {
    backgroundColor: colors.BLACK
  },
  tabHeadingText: {
    color: colors.WHITE,
    fontSize: 20
  }
})
