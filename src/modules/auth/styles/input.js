import * as colors from '../../../constants/colors'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  content: {
    backgroundColor: colors.BACKGROUND
  },
  buttonText: {
    padding: 15,
    color: colors.WHITE,
    fontSize: 20
  },
  button: {
    marginTop: 20,
    marginHorizontal: 10,
    backgroundColor: colors.BLACK,
    height: 50
  },
  firstName: {
    flex: 1
  },
  lastName: {
    flex: 1
  },
  title: {
    fontSize: 22,
    left: 20,
    marginTop: 20
  },
  headerTitle: {
    color: colors.WHITE,
    fontSize: 25,
    fontWeight: 'bold'
  },
  header: {
    backgroundColor: colors.BLACK
  },
  signupButtons: {
    bottom: 10
  }
})
