import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Container, Content, Header, Tab, Tabs, TabHeading, Text, Body, Left } from 'native-base'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'
import styles from '../styles/tabs'
import SignIn from '../components/SignIn'
import SignUp from '../components/SignUp'
import * as authActions from '../actions/actions'

export const Home = ({actions}) => (
  <Container>
    <Header hasTabs style={styles.tab}>
      <Left />
      <Body>
        <Text style={styles.headerTitle}>
          Chores to Go
        </Text>
      </Body>
    </Header>
    <Tabs>
      <Tab
        heading={
          <TabHeading style={styles.tab}>
            <Icon name='account-outline' style={styles.tabHeadingText}/>
            <Text style={styles.tabHeadingText}>Sign In</Text>
          </TabHeading>
        }>
        <SignIn signInHandler={actions.signInUser}/>
      </Tab>
      <Tab
        heading={
          <TabHeading style={styles.tab}>
            <Icon name='account-plus-outline' style={styles.tabHeadingText}/>
            <Text style={styles.tabHeadingText}>Sign Up</Text>
          </TabHeading>
        }>
        <SignUp signUpHandler={actions.signUpUser}/>
      </Tab>
    </Tabs>
  </Container>
)

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(authActions, dispatch)
})

export default connect(null, mapDispatchToProps)(Home)
