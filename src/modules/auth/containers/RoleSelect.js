import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Container, Content, Header, Text, Body, Left, Grid, Row, Button } from 'native-base'
import tabStyles from '../styles/tabs'
import buttonStyle from '../styles/input'
import * as authActions from '../actions/actions'

export const RoleSelect = ({actions}) => (
  <Container>
    <Header style={tabStyles.tab}>
      <Left />
      <Body>
        <Text style={tabStyles.headerTitle}>
          Chores to Go
        </Text>
      </Body>
    </Header>
    <Content>
      <Grid>
        <Row>
          <Button full
            style={buttonStyle.button}
            onPress={() => actions.selectCleanerRole()}>
            <Text style={buttonStyle.buttonText}>
              As Cleaner
            </Text>
          </Button>
        </Row>
        <Row>
          <Button full
            style={buttonStyle.button}
            onPress={() => actions.selectHomeOwnerRole()}>
            <Text style={buttonStyle.buttonText}>
              As Home Owner
            </Text>
          </Button>
        </Row>
      </Grid>
    </Content>
  </Container>
)

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(authActions, dispatch)
})

export default connect(null, mapDispatchToProps)(RoleSelect)
