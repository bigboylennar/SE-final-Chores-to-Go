import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Container, Content, Header, Text, Body, Left, Button } from 'native-base'
import tabStyles from '../styles/tabs'
import buttonStyle from '../styles/input'
import SkillCheckBox from '../../HomeOwner/components/ServiceSelect'
import * as authActions from '../actions/actions'

export const SkillSelect = ({actions}) => (
  <Container>
    <Header style={tabStyles.tab}>
      <Left />
      <Body>
        <Text style={tabStyles.headerTitle}>
          Chores to Go
        </Text>
      </Body>
    </Header>
    <Content>
      <Text style={buttonStyle.title}>Select Skills</Text>
      <SkillCheckBox />
      <Button full
        style={buttonStyle.button}
        onPress={() => actions.selectSkills()}>
        <Text style={buttonStyle.buttonText}>
          Done
        </Text>
      </Button>
    </Content>
  </Container>
)

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(authActions, dispatch)
})

export default connect(null, mapDispatchToProps)(SkillSelect)
