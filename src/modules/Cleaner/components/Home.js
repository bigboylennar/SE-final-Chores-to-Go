import React from 'react'
import { Container } from 'native-base'
import Header from './HomeHeader'
import Footer from './HomeFooter'
import HomeOwnerDisplay from '../containers/HomeOwnerDisplay'

export const Home = () => (
  <Container>
    <Header />
    <HomeOwnerDisplay />
    <Footer />
  </Container>
)

export default Home
