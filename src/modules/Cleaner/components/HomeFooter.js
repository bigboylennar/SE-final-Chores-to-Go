import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import styles from '../styles/footer'
import AcceptJob from '../containers/AcceptJob'
import Rating from '../containers/Rating'
import JobAction from '../containers/JobAction'
import { Footer } from 'native-base'

class HomeFooter extends Component {
  render () {
    return (
      <Footer style={styles.footer}>
        <Rating />
      </Footer>
    )
  }
}

export default HomeFooter
