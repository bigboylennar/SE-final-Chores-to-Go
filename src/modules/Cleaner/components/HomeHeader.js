import React from 'react'
import PropTypes from 'prop-types'
import styles from '../styles/header'
import { Header, Left, Body, Button, Text } from 'native-base'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'

const HomeHeader = () => (
  <Header style={styles.header}>
    <Left>
      <Button transparent>
        <Icon name='menu' style={styles.button}/>
      </Button>
    </Left>
    <Body>
      <Text style={[styles.button, styles.text]}>
        Chores to Go
      </Text>
    </Body>
  </Header>
)

export default HomeHeader
