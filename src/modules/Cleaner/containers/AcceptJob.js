import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import styles from '../styles/footer'
import * as colors from '../../../constants/colors'
import Timer from 'react-native-countdown-component'
import { Grid, Row, Button, Text } from 'native-base'

class AcceptJob extends Component {
  render () {
    return (
      <Grid>
        <Row style={styles.top}>
          <Timer
            until={15}
            size={30}
            timeToShow={['S']}
            digitBgColor={colors.BACKGROUND}
          />
        </Row>
        <Row style={styles.bottom}>
          <Button style={styles.button}>
            <Text style={styles.buttonText}>Accept Job</Text>
          </Button>
        </Row>
      </Grid>
    )
  }
}

export default AcceptJob
