import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import styles from '../styles/footer'
import { Grid, Row, Button, Text } from 'native-base'

class JobAction extends Component {
  render () {
    return (
      <Grid>
        <Row style={styles.top}>
          <Text>Start: {'Mar 1, 2018 09:00:00'}</Text>
        </Row>
        <Row style={styles.bottom}>
          <Button style={styles.button}>
            <Text style={styles.buttonText}>
              {'start job'}
            </Text>
          </Button>
        </Row>
      </Grid>
    )
  }
}

export default JobAction
