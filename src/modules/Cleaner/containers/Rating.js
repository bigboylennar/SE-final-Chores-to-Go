import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import styles from '../styles/footer'
import { Grid, Row, Col, Text } from 'native-base'
import { AirbnbRating } from 'react-native-ratings'

class Rating extends Component {
  render () {
    return (
      <Grid>
        <Row style={styles.top} pointerEvents='none'>
          <AirbnbRating
            type='custom'
            size={50}
            count={5}
            defaultRating={3}
            reviews={[]}
          />
          <Text style={styles.ratingCount}>(4)</Text>
        </Row>
        <Row style={styles.bottom}>
          <Col style={styles.bottom}>
            <Text style={styles.text}>
              Rating:
              <Text style={styles.valueText}>
                {' 4'}
              </Text>
              /5
            </Text>
          </Col>
          <Col style={styles.bottom}>
            <Text style={styles.text}>
              Hourly Rate:
              <Text style={styles.valueText}>
                {' 90'}
              </Text>
              Php
            </Text>
          </Col>
        </Row>
      </Grid>
    )
  }
}

export default Rating
