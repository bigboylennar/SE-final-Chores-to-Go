import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import styles from '../styles/content'
import * as stat from '../../../constants/status'
import * as services from '../../../constants/services'
import { View, Image } from 'react-native'
import { Text, Grid, Row, List, ListItem } from 'native-base'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'

export class HomeOwnerDisplay extends Component {
  render () {
    // const { status } = this.props
    const jobOrders = ['HOUSEKEEPING', 'CAR_WASHING', 'GARDENING', 'LAUNDRY', 'DISH_WASHING', 'PREPARE_MEALS']
    const status = stat.UNPAIRED
    switch (status) {
      case stat.UNPAIRED:
        return (
          <View style={styles.content}>
            <Text style={styles.text}>Someone is in search of you. Wait for them and do their chores.</Text>
            <Icon name='account-outline' style={styles.icon} />
          </View>
        )
      case stat.PAIRED:
        return (
          <View style={styles.content}>
            <Grid>
              <Row style={styles.avatarRow}>
                <Image
                  style={styles.avatar}
                  source={{uri: 'https://firebasestorage.googleapis.com/v0/b/chores-to-go.appspot.com/o/homeowner.png?alt=media&token=a6a0f499-7710-4285-82b7-5ef5151dd8d6'}} />
              </Row>
              <Row style={styles.infoRow}>
                <Text style={styles.nameText}>Lennar de la Puerta</Text>
              </Row>
              <Row style={styles.infoRow}>
                <Text style={styles.addressText}>
                  <Icon name='map-marker' style={styles.addressText}/>
                  Blk 3 Lot 1, Parc B, Parc Regency, Pavia, Iloilo
                </Text>
              </Row>
              <Row style={styles.jobOrdersRow}>
                <List dataArray={jobOrders}
                  renderRow={(jobOrder) =>
                    <ListItem style={styles.listItem}>
                      <Icon name={services[jobOrder].icon} style={styles.jobOrderText} />
                      <Text style={styles.jobOrderText}>{services[jobOrder].name}</Text>
                    </ListItem>
                  }>
                </List>
              </Row>
            </Grid>
          </View>
        )
      default:
        return <View />
    }
  }
}

export default HomeOwnerDisplay
