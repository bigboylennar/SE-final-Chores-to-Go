import * as colors from '../../../constants/colors'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  footer: {
    backgroundColor: colors.BACKGROUND,
    height: 200
  },
  buttonText: {
    padding: 15,
    color: colors.WHITE,
    fontSize: 20
  },
  button: {
    position: 'absolute',
    backgroundColor: colors.BLACK,
    bottom: 5,
    right: 5,
    left: 5,
    justifyContent: 'center',
    height: '100%'
  },
  top: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  ratingCount: {
    fontSize: 25
  },
  valueText: {
    fontSize: 40
  },
  text: {
    fontSize: 20
  }
})
