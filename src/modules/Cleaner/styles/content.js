import * as colors from '../../../constants/colors'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  content: {
    backgroundColor: colors.BACKGROUND,
    flex: 1,
    justifyContent: 'center'
  },
  icon: {
    color: colors.BLACK,
    fontSize: 500,
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  image: {
    resizeMode: 'center'
  },
  text: {
    textAlign: 'center',
    fontSize: 25
  },
  avatar: {
    borderRadius: 100,
    height: 200,
    width: 200,
    top: 25
  },
  avatarRow: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  infoRow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  jobOrdersRow: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listItem: {
    borderBottomWidth: 0,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  jobOrderText: {
    fontSize: 18
  },
  addressText: {
    fontSize: 22,
    marginHorizontal: 20,
    textAlign: 'center'
  },
  nameText: {
    fontSize: 25,
    marginHorizontal: 20,
    textAlign: 'center',
    fontWeight: '300'
  }
})
