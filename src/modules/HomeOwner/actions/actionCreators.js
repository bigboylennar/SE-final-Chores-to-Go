import { TOGGLE_CHECKBOX, CLEANER_FOUND } from './actionTypes'

export const toggleCheckBox = (name) => ({
  type: TOGGLE_CHECKBOX,
  name
})

export const cleanerFound = (matchedCleaner) => ({
  type: CLEANER_FOUND,
  matchedCleaner
})
