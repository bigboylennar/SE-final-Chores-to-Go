import {db} from '../../../config/firebase'
import showToast from '../../js/showToast'
import * as skills from '../../../constants/services'
import * as actions from './actionCreators'
import * as matchmaking from '../js/matchmaking'

export const match = () => {
  return async (dispatch, getState) => {
    try {
      const userRef = await db.ref('users/' + getState().currentUser.uid)
      const currentUser = await userRef.once('value')
      const cleanerRef = await db.ref('users').orderByChild('role').equalTo('CLEANER')
      await cleanerRef.on('value', snap => {
        const cleanerEntries = Object.entries(snap.val())
        const matchWithLocation = matchmaking.matchWithLocation(cleanerEntries, currentUser.val().address) // with sample address
        const matchedWithSkills = matchmaking.matchSkillsToService(matchWithLocation, getState().checkboxes)
        const randomIndex = Math.trunc((matchedWithSkills.length * Math.random()))
        const matchedCleaner = matchedWithSkills[randomIndex]
        console.log(matchedCleaner)
        if (matchedCleaner) {
          dispatch(actions.cleanerFound(matchedCleaner))
        }
      })
    } catch (error) {
      console.log(error)
    }
  }
}
