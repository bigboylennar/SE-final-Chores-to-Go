import * as colors from '../../../constants/colors'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  content: {
    backgroundColor: colors.BACKGROUND,
    flex: 1,
    justifyContent: 'center'
  },
  icon: {
    color: colors.BLACK,
    fontSize: 400,
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  image: {
    resizeMode: 'center'
  },
  text: {
    textAlign: 'center',
    fontSize: 25
  }
})
