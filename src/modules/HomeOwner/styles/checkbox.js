import * as colors from '../../../constants/colors'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  box: {
    flex: 1,
    alignContent: 'center',
    top: 3
  },
  title: {
    flex: 5,
    left: 5
  },
  text: {
    fontSize: 20,
    color: colors.BLACK
  }
})
