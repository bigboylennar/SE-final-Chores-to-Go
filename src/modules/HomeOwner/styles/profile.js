import * as colors from '../../../constants/colors'
import { StyleSheet } from 'react-native'
import { BLACK } from '../../../constants/colors';

export default StyleSheet.create({
  top: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bottom: {
    flex: 1
  },
  nameRow: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  rateRow: {
    flex: 1,
    marginLeft: 10
  },
  skillsRow: {
    flex: 5,
    marginLeft: 10
  },
  avatar: {
    borderRadius: 100,
    height: 200,
    width: 200,
    top: 30
  },
  name: {
    left: 10,
    fontSize: 25,
    fontWeight: '300'
  },
  rate: {
    fontSize: 18
  },
  imageRow: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  ratingRow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listItem: {
    borderBottomWidth: 0,
    height: 20
  },
  skillText: {
    fontSize: 17
  }
})
