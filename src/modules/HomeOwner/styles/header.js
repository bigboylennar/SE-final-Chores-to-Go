import * as colors from '../../../constants/colors'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  header: {
    backgroundColor: colors.BLACK
  },
  button: {
    fontSize: 32,
    color: colors.WHITE
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold'
  }
})
