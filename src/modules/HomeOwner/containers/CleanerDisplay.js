import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import styles from '../styles/content'
import * as stat from '../../../constants/status'
import { View } from 'react-native'
import CleanerDisplayProfile from './CleanerDisplayProfile'
import LoadScreen from '../../../components/LoadScreen'
import { Text } from 'native-base'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'

export const CleanerDisplay = ({status}) => {
  switch (status) {
    case stat.UNPAIRED:
      return (
        <View style={styles.content}>
          <Text style={styles.text}>Find someone to do your chores</Text>
          <Icon name='account-search' style={styles.icon} />
        </View>
      )
    case stat.PAIRED:
      return (
        <View style={styles.content}>
          <CleanerDisplayProfile />
        </View>
      )
    case stat.FINDING:
      return (
        <View style={styles.content}>
          <Text style={styles.text}>Finding Cleaner</Text>
          <LoadScreen />
        </View>
      )
  }
}

function mapStateToProps (state) {
  return {
    status: state.status
  }
}

export default connect(mapStateToProps)(CleanerDisplay)
