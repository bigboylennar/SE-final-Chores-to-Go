import React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from '../styles/footer'
import ServiceSelect from '../components/ServiceSelect'
import { Footer, Grid, Row, Button, Text } from 'native-base'
import * as actions from '../actions/actions'

export const HomeFooter = ({homeOwnerActions}) => (
  <Footer style={styles.footer}>
    <Grid>
      <Row style={styles.top}>
        <ServiceSelect />
      </Row>
      <Row style={styles.bottom}>
        <Button
          style={styles.button}
          onPress={() => homeOwnerActions.match()}>
          <Text style={styles.buttonText}>Find Cleaner</Text>
        </Button>
      </Row>
    </Grid>
  </Footer>
)

const mapDispatchToProps = (dispatch) => ({
  homeOwnerActions: bindActionCreators(actions, dispatch)
})

export default connect(null, mapDispatchToProps)(HomeFooter)
