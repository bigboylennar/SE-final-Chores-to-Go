import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { toggleCheckBox } from '../actions/actionCreators'
import PropTypes from 'prop-types'
import styles from '../styles/checkbox'
import { Row, Col, Text, CheckBox } from 'native-base'

export const ServiceCheckBox = ({ checkboxes, service, toggleCheckBox }) => (
  <Row>
    <Col style={styles.box}>
      <CheckBox
        color={'black'}
        checked={checkboxes.includes(service.constName)}
        onPress={() => toggleCheckBox(service.constName)}
      />
    </Col>
    <Col style={styles.title}>
      <Text style={styles.text}>
        {service.name}
      </Text>
    </Col>
  </Row>
)

ServiceCheckBox.propTypes = {
  service: PropTypes.object.isRequired,
  checkboxes: PropTypes.array.isRequired,
  toggleCheckBox: PropTypes.func.isRequired
}

function mapStateToProps (state) {
  return {
    checkboxes: state.checkboxes
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({toggleCheckBox}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceCheckBox)
