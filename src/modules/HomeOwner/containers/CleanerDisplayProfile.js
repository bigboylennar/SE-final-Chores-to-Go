import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import styles from '../styles/profile'
import * as colors from '../../../constants/colors'
import * as services from '../../../constants/services'
import { Image } from 'react-native'
import { Grid, Row, Text, List, ListItem } from 'native-base'
import { AirbnbRating } from 'react-native-ratings'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'

export const CleanerDisplayProfile = ({matchedCleaner}) => (
  <Grid>
    <Row style={styles.top}>
      <Grid>
        <Row style={styles.imageRow}>
          <Image
            style={styles.avatar}
            source={{uri: 'https://firebasestorage.googleapis.com/v0/b/chores-to-go.appspot.com/o/thumbnail.png?alt=media&token=65630cf1-1713-466e-901f-d4b0fe638f84'}} />
        </Row>
        <Row style={styles.ratingRow} pointerEvents='none'>
          <AirbnbRating
            type='custom'
            size={40}
            count={5}
            defaultRating={3}
            reviews={[]}
          />
        </Row>
      </Grid>
    </Row>
    <Row style={styles.bottom}>
      <Grid>
        <Row style={styles.nameRow}>
          <Text style={styles.name}>{matchedCleaner.firstName} {matchedCleaner.lastName}</Text>
        </Row>
        <Row style={styles.rateRow}>
          <Text style={styles.rate}>Hourly Rate: 90 Php</Text>
        </Row>
        <Row style={styles.skillsRow}>
          <Text>Strengths:</Text>
          <List dataArray={matchedCleaner.skills}
            renderRow={(skill) =>
              <ListItem style={styles.listItem}>
                <Icon name={services[skill].icon} style={styles.skillText} />
                <Text style={styles.skillText}>{services[skill].name}</Text>
              </ListItem>
            }>
          </List>
        </Row>
      </Grid>
    </Row>
  </Grid>
)

function mapStateToProps (state) {
  return {
    matchedCleaner: state.matchedCleaner
  }
}

export default connect(mapStateToProps)(CleanerDisplayProfile)
