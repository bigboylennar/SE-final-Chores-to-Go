export const matchWithLocation = (cleanersEntries, homeAddress) => {
  const matchedWithProvince = cleanersEntries.filter(cleaner => (
    cleaner[1].address.province.toLowerCase() === homeAddress.province.toLowerCase()
  ))

  if (matchedWithProvince.length > 0) {
    let matchedWithDistrict
    let matchedWithCity
    if (homeAddress.city && homeAddress.district) {
      matchedWithCity = matchedWithProvince.filter(cleaner => (
        cleaner[1].address.city.toLowerCase() === homeAddress.city.toLowerCase()
      ))
      if (matchedWithCity.length > 0) {
        matchedWithDistrict = matchedWithCity.filter(cleaner => (
          cleaner[1].address.city.toLowerCase() === homeAddress.city.toLowerCase()
        ))
        if (matchedWithDistrict.length === 0) {
          return matchedWithCity
        }
      }
      return matchedWithProvince
    } else if (homeAddress.city && !homeAddress.district) {
      matchedWithCity = matchedWithProvince.filter(cleaner => (
        cleaner[1].address.city.toLowerCase() === homeAddress.city.toLowerCase()
      ))
      if (matchedWithCity.length === 0) {
        return matchedWithProvince
      }
    } else if (!homeAddress.city && homeAddress.district) {
      matchedWithDistrict = matchedWithProvince.filter(cleaner => (
        cleaner[1].address.district.toLowerCase() === homeAddress.district.toLowerCase()
      ))
      if (matchedWithDistrict.length === 0) {
        return matchedWithProvince
      }
    }
  }
  return matchedWithProvince
}

export const matchSkillsToService = (cleanersEntries, services) => {
  const matched = cleanersEntries.filter(cleaner => {
    const skills = cleaner[1].skills ? cleaner[1].skills : {}
    return Object.values(skills).some(skill => (
      services.includes(skill)
    ))
  })
  return matched
}
