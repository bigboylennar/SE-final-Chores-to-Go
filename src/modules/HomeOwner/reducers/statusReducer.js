import { CLEANER_FOUND } from '../actions/actionTypes'
import { PAIRED, UNPAIRED } from '../../../constants/status'

const status = (state = UNPAIRED, action) => {
  switch (action.type) {
    case CLEANER_FOUND:
      return PAIRED
    default:
      return state
  }
}

export default status
