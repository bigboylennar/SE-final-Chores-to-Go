import { CLEANER_FOUND } from '../actions/actionTypes'

const status = (state = {}, action) => {
  switch (action.type) {
    case CLEANER_FOUND:
      return action.matchedCleaner[1]
    default:
      return state
  }
}

export default status
