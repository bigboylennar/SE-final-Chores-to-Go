import { TOGGLE_CHECKBOX, CLEAR_CHECKBOXES } from '../actions/actionTypes'

const checkboxes = (state = [], action) => {
  switch (action.type) {
    case TOGGLE_CHECKBOX:
      const name = action.name
      const newCheckBoxes = [...state]
      const index = newCheckBoxes.indexOf(name)
      if (index > -1) {
        newCheckBoxes.splice(index, 1)
      } else {
        newCheckBoxes.push(name)
      }
      return newCheckBoxes
    case CLEAR_CHECKBOXES:
      return []
    default:
      return state
  }
}

export default checkboxes
