import React from 'react'
import { Grid, Col } from 'native-base'
import * as services from '../../../constants/services'
import ServiceCheckBox from '../containers/ServiceCheckBox'

const ServiceSelect = () => (
  <Grid>
    <Col>
      <ServiceCheckBox service={services.HOUSEKEEPING} />
      <ServiceCheckBox service={services.LAUNDRY} />
      <ServiceCheckBox service={services.DISH_WASHING} />
    </Col>
    <Col>
      <ServiceCheckBox service={services.CAR_WASHING} />
      <ServiceCheckBox service={services.GARDENING} />
      <ServiceCheckBox service={services.PREPARE_MEALS} />
    </Col>
  </Grid>
)

export default ServiceSelect
