import React from 'react'
import { Container } from 'native-base'
import Header from './HomeHeader'
import Footer from '../containers/HomeFooter'
import Content from '../containers/CleanerDisplay'

export const Home = () => (
  <Container>
    <Header />
    <Content />
    <Footer />
  </Container>
)

export default Home
