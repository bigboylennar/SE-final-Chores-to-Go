import React from 'react'
import PropTypes from 'prop-types'
import styles from '../styles/header'
import { Header, Left, Body, Button, Text } from 'native-base'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'

const SceneHeader = ({title}) => (
  <Header style={styles.header}>
    <Left>
      <Button transparent>
        <Icon name='arrow-left' style={styles.button}/>
      </Button>
    </Left>
    <Body>
      <Text style={[styles.button, styles.text]}>
        { title }
      </Text>
    </Body>
  </Header>
)

SceneHeader.propTypes = {
  title: PropTypes.string.isRequired
}

export default SceneHeader
