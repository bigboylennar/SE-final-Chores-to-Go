import React from 'react'
import ServiceSelect from '../../components/ServiceSelect'
import {shallow} from 'enzyme'

describe('>>> Component: SERVICE SELECT', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(
      <ServiceSelect />
    )
  })

  it('+++ renders the ServiceSelect', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('+++ renders the ServiceSelect as expected', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
