import React from 'react'
import HomeHeader from '../../components/HomeHeader'
import { Left, Body } from 'native-base'
import {shallow} from 'enzyme'

describe('>>> Component: HOME HEADER', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(
      <HomeHeader />
    )
  })

  it('+++ renders the HomeHeader', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('+++ renders the HomeHeader as expected', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('+++ renders <Left />', () => {
    expect(wrapper.find(Left)).toHaveLength(1)
  })

  it('+++ renders <Body />', () => {
    expect(wrapper.find(Body)).toHaveLength(1)
  })
})
