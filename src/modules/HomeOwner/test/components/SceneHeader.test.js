import React from 'react'
import SceneHeader from '../../components/SceneHeader'
import { Left, Body } from 'native-base'
import {shallow} from 'enzyme'

describe('>>> Component: SCENE HEADER', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(
      <SceneHeader />
    )
  })

  it('+++ renders the SceneHeader', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('+++ renders the SceneHeader as expected', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('+++ renders <Left />', () => {
    expect(wrapper.find(Left)).toHaveLength(1)
  })

  it('+++ renders <Body />', () => {
    expect(wrapper.find(Body)).toHaveLength(1)
  })
})
