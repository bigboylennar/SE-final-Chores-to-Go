import React from 'react'
import Header from '../../components/HomeHeader'
import Footer from '../../containers/HomeFooter'
import Content from '../../containers/CleanerDisplay'
import Home from '../../components/Home'
import {shallow} from 'enzyme'

describe('>>> Component: HOME', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(
      <Home />
    )
  })

  it('+++ renders the Home', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('+++ renders the Home as expected', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('+++ renders <Header />', () => {
    expect(wrapper.find(Header)).toHaveLength(1)
  })

  it('+++ renders <Footer />', () => {
    expect(wrapper.find(Footer)).toHaveLength(1)
  })

  it('+++ renders <Content />', () => {
    expect(wrapper.find(Content)).toHaveLength(1)
  })
})
