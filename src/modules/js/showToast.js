import { Toast } from 'native-base'

function showToast (message) {
  Toast.show({
    text: String(message),
    position: 'bottom',
    buttonText: 'Okay'
  })
}

export default showToast
