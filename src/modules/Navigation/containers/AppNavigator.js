import React from 'react'
import { addNavigationHelpers, SwitchNavigator } from 'react-navigation'
import { createReduxBoundAddListener } from 'react-navigation-redux-helpers'
import { connect } from 'react-redux'
import Auth from '../../auth/containers/Home'
import Cleaner from '../../Cleaner/components/Home'
import HomeOwner from '../../HomeOwner/components/Home'
import CashSplitting from '../../cashSplitting/containers/TransactionList'
import SkillsSelect from '../../auth/containers/SkillsSelect'
import RoleSelect from '../../auth/containers/RoleSelect'

const routeConfig = {
  Auth: { screen: Auth },
  Cleaner: { screen: Cleaner },
  HomeOwner: { screen: HomeOwner },
  CashSplitting: { screen: CashSplitting },
  SkillsSelect: { screen: SkillsSelect },
  RoleSelect: { screen: RoleSelect }
}

const navigatorConfig = {
  initialRouteName: 'Auth'
}

export const AppNavigator = SwitchNavigator(routeConfig, navigatorConfig)

const AppWithNavigationState = ({ dispatch, nav }) => {
  const addListener = createReduxBoundAddListener('root')
  return (
    <AppNavigator
      navigation={addNavigationHelpers({ dispatch, state: nav, addListener })}
    />
  )
}

const mapStateToProps = state => ({
  nav: state.nav
})

export default connect(mapStateToProps)(AppWithNavigationState)
