import { NavigationActions } from 'react-navigation'
import { AppNavigator } from '../containers/AppNavigator'
import * as actions from '../../auth/actions/actionTypes'

const router = AppNavigator.router
const mainNavAction = router.getActionForPathAndParams('Auth')
const initialNavState = router.getStateForAction(mainNavAction)

const navReducer = (state = initialNavState, action) => {
  let nextState
  switch (action.type) {
    case actions.SIGNIN_SUCCESS:
      if (action.role === 'CLEANER') {
        nextState = router.getStateForAction(NavigationActions.navigate({ routeName: 'Cleaner' }), state)
      } else if (action.role === 'HOME_OWNER') {
        nextState = router.getStateForAction(NavigationActions.navigate({ routeName: 'HomeOwner' }), state)
      } else if (action.role === 'ADMIN') {
        nextState = router.getStateForAction(NavigationActions.navigate({ routeName: 'CashSplitting' }), state)
      }
      break
    case actions.SIGNUP_SUCCESS:
      nextState = router.getStateForAction(NavigationActions.navigate({ routeName: 'RoleSelect' }), state)
      break
    case actions.SELECT_ROLE_CLEANER:
      nextState = router.getStateForAction(NavigationActions.navigate({ routeName: 'SkillsSelect' }), state)
      break
    case actions.SELECT_ROLE_HOME_OWNER:
      nextState = router.getStateForAction(NavigationActions.navigate({ routeName: 'HomeOwner' }), state)
      break
    case actions.SELECT_SKILLS:
      nextState = router.getStateForAction(NavigationActions.navigate({ routeName: 'Cleaner' }), state)
      break
    default:
      nextState = router.getStateForAction(action, state)
      break
  }
  // console.log(nextState)
  return nextState || state
}

export default navReducer
